import { Dimensions } from "react-native";

export default function getWindowWidth() {
  return Dimensions.get("window").width;
}
