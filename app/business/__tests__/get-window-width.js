import getWindowWidth from "../get-window-width";

jest.mock("react-native", () => ({
  __esModule: true,
  Dimensions: {
    get: () => ({ width: 500 })
  }
}));

describe("get window width", function() {
  it("should return width", function() {
    expect(getWindowWidth()).toEqual(500);
  });
});
