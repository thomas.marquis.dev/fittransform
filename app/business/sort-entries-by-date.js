import { is, sort } from "ramda";

function sortEntriesByDate(a, b) {
  if (!is(Date, a) || !is(Date, b)) return 1;
  return a.date.getTime() - b.date.getTime();
}

export default sort(sortEntriesByDate);
