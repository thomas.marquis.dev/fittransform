import { is } from "ramda";

export default function formatDate(date, displayTime = false) {
  if (!is(Date, date)) return null;
  const time = displayTime ? ` - ${date.getHours()}:${date.getMinutes()}` : "";
  return `${date.getDate()}/${date.getMonth()}${time}`;
}
