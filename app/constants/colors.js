export const blue = "#429BF0";
export const grey = "#434E57";
export const cyan = "#47BDB8";
export const pinkBeige = "#C58A7B";
export const red = "#F04C42";
export const white = "#FFFFFF";
export const lightGrey = "#F2F2F2";
