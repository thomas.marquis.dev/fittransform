import React from "react";
import { StyleSheet, View } from "react-native";
import { node, string } from "prop-types";
import { Header, Text } from "react-native-elements";

import { white, red, lightGrey } from "../constants/colors";

export default function Screen({ children, title }) {
  const renderTitle = () =>
    title && (
      <Text h3 style={styles.title}>
        {title}
      </Text>
    );

  return (
    <View style={styles.container}>
      <Header
        leftComponent={{ icon: "menu", color: white }}
        centerComponent={renderTitle()}
        rightComponent={{ icon: "help", color: white }}
        containerStyle={styles.header}
      />
      {children}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: "flex-start",
    flex: 1,
    backgroundColor: lightGrey
  },
  title: {
    color: white
  },
  header: {
    backgroundColor: red,
    justifyContent: "space-around"
  }
});

Screen.propTypes = {
  children: node,
  title: string
};

Screen.defaultProps = {
  children: null,
  title: null
};
