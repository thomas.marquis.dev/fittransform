import React from "react";
import { Header, Text } from "react-native-elements";
import { shallow } from "enzyme";

import Screen from "../";
import { white } from "../../constants/colors";

describe("Screen", function() {
  it("should render top header", function() {
    const wrapper = shallow(<Screen />);

    expect(wrapper.childAt(0).type()).toEqual(Header);
    expect(wrapper.childAt(0).props()).toMatchObject({
      leftComponent: { icon: "menu", color: white },
      rightComponent: { icon: "help", color: white }
    });
  });

  it("should render title if any", function() {
    const wrapper = shallow(<Screen title="toto" />);

    expect(wrapper.childAt(0).props().centerComponent.type).toEqual(Text);
    expect(wrapper.childAt(0).props().centerComponent.props.children).toEqual(
      "toto"
    );
  });

  it("should not render title else", function() {
    const wrapper = shallow(<Screen />);
    expect(wrapper.childAt(0).props().centerComponent).toBeFalsy();
  });
});
