export const HISTORY_ADD_ENTRY = "HISTORY_ADD_ENTRY";
export const HISTORY_REMOVE_ENTRY = "HISTORY_REMOVE_ENTRY";

export function historyAddEntry(weight) {
  return {
    type: HISTORY_ADD_ENTRY,
    payload: {
      weight
    }
  };
}

export function historyRemoveEntry(id) {
  return {
    type: HISTORY_REMOVE_ENTRY,
    payload: {
      id
    }
  };
}
