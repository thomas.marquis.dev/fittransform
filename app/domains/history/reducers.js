import { filter, propEq, complement } from "ramda";

import { HISTORY_ADD_ENTRY, HISTORY_REMOVE_ENTRY } from "./actions";

const initialState = {
  entries: []
};

function addEntry(state, { weight }) {
  const date = new Date();
  const newEntry = {
    date,
    id: date.getTime(),
    weight
  };
  return {
    ...state,
    entries: [...state.entries, newEntry]
  };
}

function removeEntry(state, { id }) {
  return {
    ...state,
    entries: filter(complement(propEq("id", id)), state.entries)
  };
}

export function historyEntityReducer(
  state = initialState,
  { type, payload } = { type: null, payload: {} }
) {
  switch (type) {
    case HISTORY_ADD_ENTRY:
      return addEntry(state, payload);
    case HISTORY_REMOVE_ENTRY:
      return removeEntry(state, payload);
    default:
      return state;
  }
}
