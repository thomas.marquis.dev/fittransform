import React from "react";
import { FlatList, StyleSheet, View } from "react-native";
import { ListItem } from "react-native-elements";
import Swipeable from "react-native-gesture-handler/Swipeable";
import { arrayOf, func } from "prop-types";
import Icon from "react-native-vector-icons/Ionicons";

import Screen from "../../Screen";
import { red, white } from "../../constants/colors";
import { entryType } from "./types";
import Add from "../add";
import formatDate from "../../business/format-date";

const keyExtractor = ({ item, index }) =>
  item && item.date ? item.date.getTime() : index;

export default function History({
  entries = [],
  removeEntry = Function.prototype
}) {
  const renderSuppressItemButton = () => {
    return (
      <View style={styles.suppressItem}>
        <Icon name="md-trash" color={white} size={35} />
      </View>
    );
  };

  const handleSuppressItem = ({ id }) => () => {
    removeEntry(id);
  };

  const renderListElement = ({ item }) => {
    if (!item) return null;

    const { date, weight } = item;
    return (
      <Swipeable
        renderRightActions={renderSuppressItemButton}
        onSwipeableRightOpen={handleSuppressItem(item)}
      >
        <ListItem
          title={`${weight} kg`}
          subtitle={formatDate(date, true)}
          style={styles.item}
        />
      </Swipeable>
    );
  };

  return (
    <Screen title="Historique">
      <FlatList
        keyExtractor={keyExtractor}
        data={entries}
        renderItem={renderListElement}
      />
      <Add />
    </Screen>
  );
}

const styles = StyleSheet.create({
  input: {
    flex: 1
  },
  modal: {
    width: 100
  },
  suppressItem: {
    backgroundColor: red,
    flex: 1
  },
  item: {
    alignItems: "flex-end"
  },
  addButton: {
    paddingRight: 15,
    paddingLeft: 15
  }
});

History.propTypes = {
  entries: arrayOf(entryType),
  addEntry: func,
  removeEntry: func
};
