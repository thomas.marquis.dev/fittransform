import { exact, number, instanceOf } from "prop-types";

export const entryType = exact({
  id: number,
  weight: number,
  date: instanceOf(Date)
});
