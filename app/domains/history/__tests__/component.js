import React from "react";
import {shallow} from "enzyme";
import {FlatList} from "react-native";
import Swipeable from "react-native-gesture-handler/Swipeable";
import {ListItem} from "react-native-elements";

import History from "../component";
import Screen from "../../../Screen";
import AddButton from "../../add";
import {entries, entry} from "../../add/__tests__/fixtures";

describe("history", () => {
  it("should render screen with title", function() {
    const wrapper = shallow(<History />);

    expect(wrapper.type()).toEqual(Screen);
    expect(wrapper.props().title).toEqual("Historique");
  });

  it("should render list", function() {
    const wrapper = shallow(<History entries={entries} />);
    const flatList = wrapper.childAt(0);
    const listItem = flatList.props().renderItem({ item: entry });

    expect(flatList.type()).toEqual(FlatList);
    expect(flatList.props().keyExtractor({ item: entry })).toEqual(entry.id);

    expect(listItem.type).toEqual(Swipeable);
    expect(listItem.props.children.type).toEqual(ListItem);
  });

  it("should render add button", function() {
    const wrapper = shallow(<History />);
    expect(wrapper.childAt(1).type()).toEqual(AddButton);
  });
});
