import { is, values, uniq, keys } from "ramda";

import * as actions from "../actions";
import { entryId, entry, weight } from "../../add/__tests__/fixtures";

describe("history actions", function() {
  const actionTypes = [];
  const actionCreators = [];
  values(actions).forEach(action => {
    if (is(String, action)) {
      actionTypes.push(action);
    } else {
      actionCreators.push(action);
    }
  });

  it("should have unique action types", function() {
    expect(uniq(actionTypes).length).toEqual(actionTypes.length);
  });

  it("should create action with payload", function() {
    actionCreators.forEach(actionCreator => {
      expect(keys(actionCreator(null))).toContain("payload");
    });
  });

  describe("add entry action creator", function() {
    it("should build action", function() {
      expect(actions.historyAddEntry(weight)).toEqual({
        type: actions.HISTORY_ADD_ENTRY,
        payload: { weight }
      });
    });
  });

  describe("remove entry action creator", function() {
    it("should build action", function() {
      expect(actions.historyRemoveEntry(entryId)).toEqual({
        type: actions.HISTORY_REMOVE_ENTRY,
        payload: { id: entryId }
      });
    });
  });
});
