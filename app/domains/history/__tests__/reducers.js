import { historyEntityReducer } from "../reducers";
import { historyAddEntry, historyRemoveEntry } from "../actions";
import { entries, entryId, entry2, entry, date, weight } from "../../add/__tests__/fixtures";

global.Date = jest.fn(() => date);

const state = {
  entries
};
const littleState = {
  entries: [entry2]
};

describe("history reducers", function() {
  describe("historyEntityReducer", function() {
    it("should return default state without action", function() {
      expect(historyEntityReducer()).toEqual({
        entries: []
      });
    });

    it("should remove entry", function() {
      expect(historyEntityReducer(state, historyRemoveEntry(entryId))).toEqual(
        littleState
      );
    });

    it("should add entry", function() {
      expect(
        historyEntityReducer(littleState, historyAddEntry(weight))
      ).toEqual({ entries: [entry2, entry] });
    });
  });
});
