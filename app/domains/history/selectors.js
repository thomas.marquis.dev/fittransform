import { pathOr, compose, filter, has } from "ramda";

const emptyArray = [];

export const selectAllEntriesFromHistory = compose(
  filter(has("id")),
  pathOr(emptyArray, ["entities", "history", "entries"])
);
