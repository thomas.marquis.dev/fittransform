import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { reverse } from "ramda";

import History from "./component";
import { selectAllEntriesFromHistory } from "./selectors";
import { historyRemoveEntry } from "./actions";
import sortEntriesByDate from "../../business/sort-entries-by-date";

function mapStateToProps(state) {
  return {
    entries: reverse(sortEntriesByDate(selectAllEntriesFromHistory(state)))
  };
}

function mapDispatchToProps(dispatch) {
  return {
    removeEntry: bindActionCreators(historyRemoveEntry, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(History);
