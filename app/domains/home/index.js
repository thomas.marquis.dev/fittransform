import { connect } from "react-redux";

import { selectAllEntriesFromHistory } from "../history/selectors";
import Home from "./component";
import sortEntriesByDate from "../../business/sort-entries-by-date";

function mapStateToProps(state) {
  return {
    entries: sortEntriesByDate(selectAllEntriesFromHistory(state))
  };
}

export default connect(mapStateToProps)(Home);
