import React from "react";
import { arrayOf } from "prop-types";
import { LineChart } from "react-native-chart-kit";
import { isEmpty, map, prop } from "ramda";
import { StyleSheet, View } from "react-native";

import { entryType } from "../history/types";
import { grey, lightGrey } from "../../constants/colors";
import getWindowWidth from "../../business/get-window-width";
import formatDate from "../../business/format-date";

const mapToLabels = map(({ date }) => formatDate(date));
const mapToData = map(prop("weight"));

export default function Chart({ entries = [] }) {
  const chartConfig = {
    backgroundColor: lightGrey,
    backgroundGradientFrom: lightGrey,
    backgroundGradientTo: lightGrey,
    decimalPlaces: 1,
    color: () => grey,
    propsForDots: {
      r: "6",
      strokeWidth: "0"
    }
  };

  return (
    <View style={styles.chartBloc}>
      {!isEmpty(entries) && (
        <LineChart
          data={{
            labels: mapToLabels(entries),
            datasets: [{ data: mapToData(entries) }]
          }}
          width={getWindowWidth()}
          height={300}
          chartConfig={chartConfig}
        />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  chartBloc: {
    paddingTop: 15,
    flex: 2
  }
});

Chart.propTypes = {
  entries: arrayOf(entryType)
};
