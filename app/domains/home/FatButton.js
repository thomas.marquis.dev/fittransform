import React from "react";
import { StyleSheet } from "react-native";
import { func, bool } from "prop-types";
import { Button } from "react-native-elements";
import { red } from "../../constants/colors";

export default function FatButton({
  renderImage = Function.prototype,
  onPress = Function.prototype,
  isSelected = false
}) {
  const getStyle = () =>
    isSelected
      ? {
          ...styles.button,
          backgroundColor: red
        }
      : styles.button;

  return (
    <Button
      onPress={onPress}
      type={isSelected ? "solid" : "outline"}
      icon={renderImage(isSelected)}
      buttonStyle={getStyle()}
    />
  );
}

const styles = StyleSheet.create({
  button: {
    flex: 1,
    maxHeight: 150,
    marginLeft: 15,
    marginTop: 15
  }
});

FatButton.propTypes = {
  renderImage: func,
  onPress: func,
  isSelected: bool
};
