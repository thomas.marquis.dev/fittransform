import React from "react";
import { shallow } from "enzyme";
import { LineChart } from "react-native-chart-kit";

import Chart from "../Chart";
import { entries } from "../../add/__tests__/fixtures";
import { entryValues, formattedLabels } from "./fixtures";
import { grey, lightGrey } from "../../../constants/colors";

const windowWidth = 500;

jest.mock("../../../business/get-window-width", () => ({
  __esModule: true,
  default: () => 500
}));

describe("Chart", function() {
  it("should render LineChart", function() {
    const wrapper = shallow(<Chart entries={entries} />);
    expect(wrapper.childAt(0).type()).toEqual(LineChart);
  });

  it("should configure chart correctly", function() {
    const wrapper = shallow(<Chart entries={entries} />);
    const chartProps = wrapper.childAt(0).props();

    expect(chartProps.chartConfig).toEqual({
      backgroundColor: lightGrey,
      backgroundGradientFrom: lightGrey,
      backgroundGradientTo: lightGrey,
      decimalPlaces: 1,
      color: expect.any(Function),
      propsForDots: {
        r: "6",
        strokeWidth: "0"
      }
    });
    expect(chartProps.chartConfig.color(null)).toEqual(grey);
    expect(chartProps.height).toEqual(300);
    expect(chartProps.width).toEqual(windowWidth);
  });

  it("should map data", function() {
    const wrapper = shallow(<Chart entries={entries} />);
    expect(wrapper.childAt(0).props().data.datasets[0].data).toEqual(
      entryValues
    );
  });

  it("should map labels", function() {
    const wrapper = shallow(<Chart entries={entries} />);
    expect(wrapper.childAt(0).props().data.labels).toEqual(formattedLabels);
  });

  it("should return empty view without entries", function() {
    const wrapper = shallow(<Chart />);
    expect(wrapper.childAt(0).length).toEqual(0);
  });
});
