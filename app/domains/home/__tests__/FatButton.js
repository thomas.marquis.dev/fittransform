import React from "react";
import { shallow } from "enzyme";
import { Icon, Button } from "react-native-elements";

import FatButton from "../FatButton";

describe("FatButton", function() {
  it("should render Button", function() {
    const wrapper = shallow(<FatButton />);

    expect(wrapper.at(0).type()).toEqual(Button);
  });

  it("should render icon", function() {
    const renderIconMock = jest.fn(() => <Icon name="male" type="ionicon" />);
    const wrapper = shallow(<FatButton renderImage={renderIconMock} />);

    expect(renderIconMock).toBeCalled();
    expect(wrapper.props().icon.props).toMatchObject({
      name: "male",
      type: "ionicon"
    });
  });

  it("should call handler on press", function() {
    const mockHandler = jest.fn();
    const wrapper = shallow(<FatButton onPress={mockHandler} />);
    const mockEvent = { nativeEven: "foo" };
    wrapper.simulate("press", mockEvent);

    expect(mockHandler).toBeCalledWith(mockEvent);
  });

  it("should use outline button type when not selected", function() {
    const renderIconMock = jest.fn();
    const wrapper = shallow(<FatButton isSelected={false} />);

    expect(wrapper.at(0).props().type).toEqual("outline");
  });

  it("should use outline button type when not selected", function() {
    const renderIconMock = jest.fn();
    const wrapper = shallow(<FatButton isSelected />);

    expect(wrapper.at(0).props().type).toEqual("solid");
  });
});
