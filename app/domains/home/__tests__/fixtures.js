import { entries } from "../../add/__tests__/fixtures";

export const formattedLabels = ["1/1", "1/2"];
export const entryValues = [entries[0].weight, entries[1].weight];
