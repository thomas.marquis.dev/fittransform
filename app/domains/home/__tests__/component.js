import React from "react";
import { shallow } from "enzyme";
import { Slider, Text } from "react-native-elements";
import Icon from "react-native-vector-icons/Ionicons";

import FatButton from "../FatButton";
import Home from "../component";
import Screen from "../../../Screen";
import Chart from "../Chart";
import { entries } from "../../add/__tests__/fixtures";
import { grey, white } from "../../../constants/colors";

describe("Home", function() {
  it("should render Screen at the top level", function() {
    const wrapper = shallow(<Home />);
    expect(wrapper.at(0).type()).toEqual(Screen);
    expect(wrapper.at(0).props().title).toEqual("Accueil");
  });

  it("should render 2 buttons with icons", function() {
    const wrapper = shallow(<Home />);
    const buttons = wrapper
      .at(0)
      .childAt(0)
      .children();

    expect(buttons).toHaveLength(2);
    expect(buttons.at(0).type()).toEqual(FatButton);
    expect(buttons.at(1).type()).toEqual(FatButton);
  });

  it("should render gender button icons", function() {
    const wrapper = shallow(<Home />);
    const buttons = wrapper
      .at(0)
      .childAt(0)
      .children();
    const isSelected = true;
    const icon1 = buttons
      .at(0)
      .props()
      .renderImage(isSelected);
    const icon2 = buttons
      .at(1)
      .props()
      .renderImage(!isSelected);

    expect(icon1.props).toMatchObject({
      name: "md-female",
      size: 100,
      color: white
    });
    expect(icon2.props).toMatchObject({
      name: "md-male",
      size: 100,
      color: grey
    });

    expect(icon1.type).toEqual(Icon);
    expect(icon2.type).toEqual(Icon);
  });

  it("should select first button by default", function() {
    const wrapper = shallow(<Home />);
    const buttons = wrapper
      .at(0)
      .childAt(0)
      .children();

    expect(buttons.at(0).props().isSelected).toEqual(true);
    expect(buttons.at(1).props().isSelected).toEqual(false);
  });

  it("should unselect first button when second pressed", function() {
    const wrapper = shallow(<Home />);
    wrapper
      .childAt(0)
      .childAt(1)
      .props()
      .onPress();
    const buttons = wrapper.childAt(0).children();

    expect(buttons.at(1).props().isSelected).toEqual(true);
    expect(buttons.at(0).props().isSelected).toEqual(false);
  });

  it("should render slider with default values", function() {
    const wrapper = shallow(<Home />);
    const sliderBloc = wrapper.childAt(1);
    const slider = sliderBloc.childAt(0);
    const sliderText = sliderBloc.childAt(1);

    expect(slider.type()).toEqual(Slider);
    expect(sliderText.type()).toEqual(Text);

    expect(slider.props().value).toEqual(0.5);
    expect(sliderText.childAt(0).text()).toEqual("75 kg");
  });

  it("should handle slider on change value", function() {
    const wrapper = shallow(<Home />);
    wrapper
      .childAt(1)
      .childAt(0)
      .props()
      .onValueChange(1);

    const sliderBloc = wrapper.childAt(1);
    const slider = sliderBloc.childAt(0);
    const sliderText = sliderBloc.childAt(1);

    expect(slider.props().value).toEqual(1);
    expect(sliderText.childAt(0).text()).toEqual("150 kg");
  });

  it("should render chart with entries", function() {
    const wrapper = shallow(<Home entries={entries} />);

    expect(wrapper.childAt(2).type()).toEqual(Chart);
    expect(wrapper.childAt(2).props()).toEqual({ entries });
  });
});
