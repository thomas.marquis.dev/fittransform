import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import { arrayOf } from "prop-types";
import Icon from "react-native-vector-icons/Ionicons";
import { Slider, Text } from "react-native-elements";

import { entryType } from "../history/types";
import Screen from "../../Screen";
import { grey, red, white } from "../../constants/colors";
import FatButton from "./FatButton";
import Chart from "./Chart";
import Add from "../add";

function renderGenderIcon(gender) {
  return isSelected => (
    <Icon name={`md-${gender}`} size={100} color={isSelected ? white : grey} />
  );
}

const genders = {
  FEMALE: "female",
  MALE: "male"
};

const MAX_WEIGHT = 150;

export default function Home({ entries = [] }) {
  const [selectedGender, setSelectedGender] = useState(genders.FEMALE);
  const [selectedWeight, setSelectedWeight] = useState(75);

  const handleSwitchGender = gender => () => {
    setSelectedGender(gender);
  };

  const renderGenderButton = gender => (
    <FatButton
      renderImage={renderGenderIcon(gender)}
      isSelected={gender === selectedGender}
      onPress={handleSwitchGender(gender)}
    />
  );

  return (
    <Screen title="Accueil">
      <View style={styles.buttonInputs}>
        {renderGenderButton(genders.FEMALE)}
        {renderGenderButton(genders.MALE)}
      </View>
      <View style={styles.sliderInput}>
        <Slider
          value={selectedWeight / MAX_WEIGHT}
          onValueChange={value => setSelectedWeight(value * MAX_WEIGHT)}
          thumbStyle={{ backgroundColor: red }}
          minimumTrackTintColor={grey}
        />
        <Text style={styles.sliderText}>{`${Math.round(
          selectedWeight
        )} kg`}</Text>
      </View>
      <Chart entries={entries} />
      <Add />
    </Screen>
  );
}

const styles = StyleSheet.create({
  buttonInputs: {
    flexDirection: "row",
    flex: 1
  },
  sliderInput: {
    paddingLeft: 15,
    paddingRight: 15,
    alignItems: "stretch",
    justifyContent: "center"
  },
  sliderText: {
    color: grey,
    alignSelf: "center",
    fontSize: 27
  }
});

Home.propTypes = {
  entries: arrayOf(entryType)
};
