import React from "react";
import { StyleSheet } from "react-native";
import { Image, Text } from "react-native-elements";

import Screen from "../../Screen";

export default function Infos() {
  return (
    <Screen title="Infos">
      <Image
        source={require("../../../assets/imc.png")}
        style={{ width: "100%", height: 200 }}
      />
      <Text style={styles.text}>
        L’indice de masse corporelle (IMC) permet d’évaluer rapidement votre
        corpulence simplement avec votre poids et votre taille, quel que soit
        votre sexe. Calculez rapidement votre IMC et découvrez dans quelle
        catégorie vous vous situez. L’indice de masse corporelle (IMC) est le
        seul indice validé par l’Organisation mondiale de la santé pour évaluer
        la corpulence d’un individu et donc les éventuels risques pour la santé.
        L’IMC permet de déterminer si l’on est situation de maigreur, de
        surpoids ou d’obésité par exemple.
      </Text>
    </Screen>
  );
}

const styles = StyleSheet.create({
  image: {
    flexDirection: "column",
    flex: 1
  },
  text: {
    paddingLeft: 15,
    paddingRight: 15,
    fontSize: 17,
    textAlign: "justify"
  }
});
