import React from "react";
import { shallow } from "enzyme";
import { Image, Text, ListItem } from "react-native-elements";

import Infos from "../index";

describe("infos", () => {
  it("should have two children", function() {
    const wrapper = shallow(<Infos />);
    expect(wrapper.children().length).toEqual(2);
  });

  it("should display image on top", () => {
    const wrapper = shallow(<Infos />);
    expect(
      wrapper
        .children()
        .at(0)
        .type()
    ).toBe(Image);
  });

  it("should display text bloc", () => {
    const wrapper = shallow(<Infos />);
    expect(
      wrapper
        .children()
        .at(1)
        .type()
    ).toBe(Text);
  });
});
