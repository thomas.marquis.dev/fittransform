import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import Add from "./component";
import { historyAddEntry } from "../history/actions";

function mapDispatchToProps(dispatch) {
  return {
    addEntry: bindActionCreators(historyAddEntry, dispatch)
  };
}

export default connect(null, mapDispatchToProps)(Add);
