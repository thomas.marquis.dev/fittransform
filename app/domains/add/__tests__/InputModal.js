import React from "react";
import { shallow } from "enzyme";
import { Text, Input, Overlay, Button } from "react-native-elements";

import InputModal from "../InputModal";

describe("InputModal", function() {
  it("should render overlay and his content when is open", function() {
    const wrapper = shallow(<InputModal isOpen />);

    expect(wrapper.type()).toEqual(Overlay);

    expect(wrapper.childAt(0).childAt(0).type()).toEqual(Text);
    expect(
      wrapper
        .childAt(0) // View
        .childAt(0) // Text
        .childAt(0) // innerText node
        .text() // text content
    ).toEqual("Saisi ton poids");

    expect(wrapper.childAt(0).childAt(1).type()).toEqual(Input);
    expect(wrapper.childAt(0).childAt(1).props()).toMatchObject({
      keyboardType: "number-pad",
      placeholder: "95.5",
      bottomDivider: true
    });

    expect(wrapper.childAt(0).childAt(2).type()).toEqual(Button);
    expect(wrapper.childAt(0).childAt(2).props()).toMatchObject({
      title: "Go !",
      type: "solid",
      onPress: expect.any(Function)
    });
  });

  it("should handle on validate", function() {
    const onValidateMock = jest.fn();
    const inputValue = 95;
    const changeEvent = {
      nativeEvent: { text: inputValue }
    };
    const wrapper = shallow(<InputModal isOpen onValidate={onValidateMock} />);

    wrapper
      .childAt(0)
      .childAt(1)
      .props()
      .onChange(changeEvent);
    wrapper
      .childAt(0)
      .childAt(2)
      .simulate("press");

    expect(onValidateMock).toBeCalledWith(inputValue);
  });

  it("should handle on close", function() {
    const onCloseMock = jest.fn();
    const wrapper = shallow(<InputModal isOpen onClose={onCloseMock} />);

    wrapper.props().onBackdropPress();

    expect(onCloseMock).toBeCalledTimes(1);
  });
});
