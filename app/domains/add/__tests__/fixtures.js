export const date = new Date(2020, 1, 1);
export const date2 = new Date(2020, 2, 1);

export const weight = 95;
export const entryId = date.getTime();
export const entry = {
  id: entryId,
  date,
  weight
};

export const entry2 = {
  id: date2.getTime(),
  date: date2,
  weight: 96
};

export const entries = [entry, entry2];
