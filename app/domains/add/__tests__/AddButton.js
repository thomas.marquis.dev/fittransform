import React from "react";
import { shallow } from "enzyme";
import { Button } from "react-native-elements";
import Icon from "react-native-vector-icons/Ionicons";

import AddButton from "../AddButton";
import { white } from "../../../constants/colors";

describe("AddButton", function() {
  it("should render button", function() {
    const wrapper = shallow(<AddButton />);

    expect(wrapper.type()).toEqual(Button);
    expect(wrapper.props()).toMatchObject({
      onPress: Function.prototype
    });
    expect(wrapper.props().icon.type).toEqual(Icon);
    expect(wrapper.props().icon.props).toMatchObject({
      name: "md-add",
      color: white,
      size: 25
    });
  });

  it("should call onPress", function() {
    const onPressMock = jest.fn();
    const mockEvent = { foo: "baz" };
    const wrapper = shallow(<AddButton onPress={onPressMock} />);

    wrapper.simulate("press", mockEvent);

    expect(onPressMock).toBeCalledWith(mockEvent);
  });
});
