import React from "react";
import { shallow } from "enzyme";

import Add from "../component";

describe("add", () => {
  it("should be closed by default", () => {
    const wrapper = shallow(<Add />);

    expect(wrapper.childAt(1).props().isOpen).toEqual(false);
  });

  it("should open modal if add button pressed", function() {
    const wrapper = shallow(<Add />);

    wrapper.childAt(0).simulate("press");

    expect(wrapper.childAt(1).props().isOpen).toEqual(true);
  });

  it("should close modal", function() {
    const wrapper = shallow(<Add />);

    wrapper.childAt(1).simulate("close");

    expect(wrapper.childAt(1).props().isOpen).toEqual(false);
  });
});
