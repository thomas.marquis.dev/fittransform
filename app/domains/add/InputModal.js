import React, { useState } from "react";
import { bool, func } from "prop-types";
import { Text, Input, Overlay, Button } from "react-native-elements";
import { View, StyleSheet } from "react-native";

import { red, white } from "../../constants/colors";

export default function InputModal({
  isOpen = false,
  onValidate = Function.prototype,
  onClose = Function.prototype
}) {
  const [inputWeight, setInputWeight] = useState(0);

  const handleInputChange = ({ nativeEvent }) => {
    setInputWeight(nativeEvent.text);
  };

  const handleAddEntry = () => {
    onValidate(inputWeight);
    onClose();
  };

  if (!isOpen) return null;

  return (
    <Overlay
      style={styles.modal}
      isVisible
      width="auto"
      height="auto"
      onBackdropPress={onClose}
    >
      <View>
        <Text h4>Saisi ton poids</Text>
        <Input
          keyboardType="number-pad"
          placeholder="95.5"
          style={styles.input}
          bottomDivider
          onChange={handleInputChange}
          containerStyle={styles.input}
        />
        <Button
          onPress={handleAddEntry}
          title="Go !"
          type="solid"
          buttonStyle={styles.validationButton}
        />
      </View>
    </Overlay>
  );
}

const styles = StyleSheet.create({
  validationButton: {
    marginTop: 15,
    backgroundColor: red,
    color: white,
    borderColor: red,
    fontSize: 25,
    minWidth: 80
  },
  input: {
    width: 150
  }
});

InputModal.propTypes = {
  isOpen: bool,
  onValidate: func,
  onClose: func
};
