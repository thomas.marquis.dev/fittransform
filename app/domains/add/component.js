import React, { useState } from "react";

import AddButton from "./AddButton";
import InputModal from "./InputModal";
import { func } from "prop-types";
import { StyleSheet } from "react-native";

export default function Add({
  addEntry = Function.prototype,
  onButtonPress = Function.prototype,
  onEntryAddSuccess = Function.prototype
}) {
  const [modalOpen, setModalOpen] = useState(false);

  const handleAddButtonPress = () => {
    setModalOpen(true);
    onButtonPress();
  };

  const handleClose = () => {
    setModalOpen(false);
  };

  const handleModalValidate = weight => {
    addEntry(weight);
    onEntryAddSuccess();
  };

  return (
    <>
      <AddButton onPress={handleAddButtonPress} style={styles.addButton} />
      <InputModal
        isOpen={modalOpen}
        onValidate={handleModalValidate}
        onClose={handleClose}
      />
    </>
  );
}

const styles = StyleSheet.create({
  addButton: {
    paddingRight: 15,
    paddingLeft: 15
  }
});

Add.propTypes = {
  addEntry: func,
  removeEntry: func,
  onButtonPress: func
};
