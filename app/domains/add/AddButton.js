import { func, any } from "prop-types";
import { Button } from "react-native-elements";
import React from "react";
import Icon from "react-native-vector-icons/Ionicons";

import { white, blue } from "../../constants/colors";
import { StyleSheet } from "react-native";

export default function AddButton({ onPress = Function.prototype, style }) {
  const renderAddIcon = () => <Icon name="md-add" color={white} size={25} />;

  return (
    <Button
      icon={renderAddIcon()}
      buttonStyle={{ ...styles.button, ...style }}
      onPress={onPress}
    />
  );
}

const styles = StyleSheet.create({
  button: {
    borderRadius: 25,
    width: 50,
    height: 50,
    alignSelf: "flex-end",
    marginRight: 10,
    backgroundColor: blue
  }
});

AddButton.propTypes = {
  onPress: func,
  style: any
};
