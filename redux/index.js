import { AsyncStorage } from "react-native";
import { combineReducers, createStore } from "redux";
import { persistStore, persistReducer } from "redux-persist";

import { historyEntityReducer } from "../app/domains/history/reducers";

const rootReducer = combineReducers({
  entities: combineReducers({
    history: historyEntityReducer
  })
});

const persistConfig = {
  key: "root",
  storage: AsyncStorage,
  whitelist: ["entities"],
  blacklist: ["navigation"]
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = createStore(persistedReducer);

export const persistor = persistStore(store);
