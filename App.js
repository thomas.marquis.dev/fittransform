import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { PersistGate } from "redux-persist/es/integration/react";
import { cond, always, equals, T } from "ramda";
import { Provider } from "react-redux";

import HomeScreen from "./app/domains/home";
import InfoScreen from "./app/domains/infos";
import HistoryScreen from "./app/domains/history";
import { INFOS, HOME, HISTORY } from "./app/routes";
import { red, grey } from "./app/constants/colors";
import Icon from "react-native-vector-icons/Ionicons";
import { persistor, store } from "./redux";

const Tab = createBottomTabNavigator();

function getIconName(route, foccused) {
  return cond([
    [equals(HOME), always(foccused ? "md-home" : "md-home")],
    [
      equals(INFOS),
      always(
        foccused ? "md-information-circle" : "md-information-circle-outline"
      )
    ],
    [equals(HISTORY), always("md-add")],
    [T, always("md-ellipsis-horizontal")]
  ])(route);
}

function renderAppContent() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => (
            <Icon
              name={getIconName(route.name, focused)}
              color={color}
              size={size}
            />
          )
        })}
        tabBarOptions={{
          activeTintColor: red,
          inactiveTintColor: grey
        }}
      >
        <Tab.Screen
          name={HOME}
          component={HomeScreen}
          options={{ tabBarLabel: "Accueil" }}
        />
        <Tab.Screen
          name={INFOS}
          component={InfoScreen}
          options={{ tabBarLabel: "Infos" }}
        />
        <Tab.Screen
          name={HISTORY}
          component={HistoryScreen}
          options={{ tabBarLabel: "Historique" }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
}

export default function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        {renderAppContent()}
      </PersistGate>
    </Provider>
  );
}
