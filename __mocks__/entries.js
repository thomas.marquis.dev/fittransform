import { range, map } from "ramda";

function generateOne(weight, year, month, day) {
  const date = new Date(year, month, day);

  return {
    id: date.getTime(),
    date,
    weight
  };
}

export default function generate(nb) {
  return map(
    i => generateOne(50 + i * 0.5, 2020, 1, i),
    range(0, nb > 30 ? 30 : nb)
  );
}
